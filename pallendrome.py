def is_palendrome(s: str) -> bool:
    return s == s[::-1] and s != ""


def solution(S: str) -> str:
    x = 0
    length = len(S)
    while True:
        y = x + length
        substr = S[x:y]
        if is_palendrome(substr):
            return substr
        if y + 1 > len(S):
            length -= 1
            x = 0
        else:
            x += 1


try:
    assert solution("a") == "a", "test 1 should be a"
    assert solution("aaaab") == "aaaa", "test 2 should be aaaa"
    assert solution("baaaa") == "aaaa", "test 3 should be aaaa"
    assert solution("babcbad") == "abcba", "test 4 should be abcba"
    assert solution("aaabcccc") == "cccc", "test 5 should be cccc"
    assert solution("abcdefgh") == "a", "test 6 should be a"
except Exception as e:
    print(f"fail: {e}")
    raise (e)
else:
    print("success")
